package com.example.projectchatbot;


class APIQueryGenerator {
    private final String Latitude;
    private final String Longitude;
    APIQueryGenerator()
    {
        this.Latitude = "";
        this.Longitude = "";
    }
    APIQueryGenerator(String Latitude, String Longitude)
    {
        this.Latitude = Latitude;
        this.Longitude = Longitude;
    }

    StringBuilder generator(String place, int type)
    {
        StringBuilder stringBuilder = new StringBuilder();
        String API_KEY = "AIzaSyBtaX--WkbbL_Aajd4_W-8knV3VwAoxZ-I";
        if(type == 1)
        {
            stringBuilder.append("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
            stringBuilder.append("key=").append(API_KEY);
            stringBuilder.append("&keyword=").append(place);
            stringBuilder.append("&location=").append(Latitude).append(",").append(Longitude);
            stringBuilder.append("&rankby=distance");
            stringBuilder.append("&language=zh-TW");
        }
        else if(type == 2)
        {
            stringBuilder.append("https://maps.googleapis.com/maps/api/place/details/json?");
            stringBuilder.append("&key=").append(API_KEY);
            stringBuilder.append("&placeid=").append(place);
            stringBuilder.append("&language=zh-TW");
        }
        else if(type == 3)
        {
            stringBuilder.append("https://maps.googleapis.com/maps/api/geocode/json?");
            stringBuilder.append("latlng=").append(Latitude).append(",").append(Longitude).append("&key=").append(API_KEY);
        }


        return stringBuilder;
    }

}
