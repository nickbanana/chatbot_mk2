package com.example.projectchatbot;

import android.os.AsyncTask;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import okhttp3.OkHttpClient;
import okhttp3.Request;

/**
 * Created by nickbanana on 2016/12/12.
 */

class AsyncTextSearch extends AsyncTask <String, Integer, POIDetail> {

    @Override
    protected POIDetail doInBackground(String... url) {
        OkHttpClient okHttpClient = new OkHttpClient();
        JsonObject object;
        POIDetail data = new POIDetail();
        Request request = new Request.Builder()
                .url(url[0])
                .build();

        try
        {
            okhttp3.Response response = okHttpClient.newCall(request).execute();
            if(response.code() == 200)
            {
                object = new JsonParser().parse(response.body().string()).getAsJsonObject();
                String ID = object.getAsJsonObject("results").get("place_id").getAsString();
                data.Setvariable(1,ID);
            }
            return data;
        }
        catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
            POIDetail t = new POIDetail("error");
            t.Setvariable(6,e.getMessage());
            return t;
        }
        catch (IOException e)
        {
            e.printStackTrace();
            POIDetail t = new POIDetail("error");
            t.Setvariable(6,e.getMessage());
            return t;
        }
    }



}
