package com.example.projectchatbot;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.Serializable;
import java.util.concurrent.ExecutionException;

/**
 * Created by nickbanana on 2016/10/8.
 */

class POIDetail implements Serializable{
    private String place_id;
    private String name;
    private String Lat;
    private String Lng;
    private String Formatted_Address;
    private String telephone;
    //private JsonObject RawData;
    private String Error_Message;

    public POIDetail()
    {
        this.place_id = "";
    }




    public POIDetail(String id)
    {
        this.place_id = id;

    }

    void FillParam()
    {
        APIQueryGenerator apiQueryGenerator = new APIQueryGenerator();
        AsyncQuery asyncQuery = new AsyncQuery();
        asyncQuery.execute(apiQueryGenerator.generator(place_id,0).toString());
        try {
            Parser(new JsonParser().parse(asyncQuery.get()).getAsJsonObject());
        }catch (InterruptedException | ExecutionException e)
        {
            e.getStackTrace();
        }

    }

    String Getvariable(int switcher)
    {
        switch (switcher)
        {
            case 1: return place_id;
            case 2: return name;
            case 3: return Lat;
            case 4: return Lng;
            case 5: return Formatted_Address;
            case 6: return Error_Message;
            case 7: return telephone;
        }
        return "";
    }

    void Setvariable(int switcher, String var)
    {
        switch (switcher)
        {
            case 1: this.place_id = var; break;
            case 2: this.name = var; break;
            case 3: this.Lat = var; break;
            case 4: this.Lng = var; break;
            case 5: this.Formatted_Address = var; break;
            case 6: this.Error_Message = var; break;
            case 7: this.telephone = var; break;
        }

    }

    private void Parser(JsonObject data)
    {
        Formatted_Address = data.getAsJsonObject("result").get("formatted_address").getAsString();
        name = data.getAsJsonObject("result").get("name").getAsString();
        Lng = data.getAsJsonObject("result").getAsJsonObject("geometry").getAsJsonObject("location").get("lng").getAsString();
        Lat = data.getAsJsonObject("result").getAsJsonObject("geometry").getAsJsonObject("location").get("lat").getAsString();
        if(data.getAsJsonObject("result").has("formatted_phone_number"))
        {
            telephone = data.getAsJsonObject("result").get("formatted_phone_number").toString().replaceAll("\"","").replaceAll(" ","");
        }
        else
            telephone = "";

    }

}
