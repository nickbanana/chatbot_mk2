package com.example.projectchatbot;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by nickbanana on 2016/10/12.
 */

class DataWrapper implements Serializable {
    private ArrayList<POIDetail> poiDetailsList;

    public DataWrapper(ArrayList<POIDetail> poiDetailsList)
    {
        this.poiDetailsList = poiDetailsList;
    }

    public ArrayList<POIDetail> getPoiDetailsList()
    {
        return this.poiDetailsList;
    }
}
