package com.example.projectchatbot;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.Request;


class AsyncFetchQuery extends AsyncTask<String, Integer, ArrayList<POIDetail> > {


        @Override
        protected ArrayList<POIDetail> doInBackground(String... url)
        {
            OkHttpClient okHttpClient = new OkHttpClient();
            JsonObject object;
            Request request = new Request.Builder()
            .url(url[0])
            .build();
            Log.v("url",url[0]);
            try
            {
                okhttp3.Response response = okHttpClient.newCall(request).execute();
                if(response.code()==200)
                {
                    object = new JsonParser().parse(response.body().string()).getAsJsonObject();
                    ArrayList<POIDetail> result = new ArrayList<>();
                    JsonArray result_array = object.getAsJsonArray("results");
                    for (int i = 0; i < result_array.size(); i++)
                    {
                        String ID = result_array.get(i).getAsJsonObject().get("place_id").getAsString();
                        String Name = result_array.get(i).getAsJsonObject().get("name").getAsString();

                        Log.v("ID NAME", ID+ " " + Name);
                        POIDetail tmp = new POIDetail(ID);
                        result.add(tmp);
                    }
                    return result;


                }
                else
                {
                    POIDetail t = new POIDetail("error");
                    t.Setvariable(6,"Error on response code");
                    ArrayList<POIDetail> tmparray = new ArrayList<>();
                    tmparray.add(t);
                    return tmparray;
                }

            }catch (UnsupportedEncodingException  e)
            {
                e.printStackTrace();
                POIDetail t = new POIDetail("error");
                t.Setvariable(6,e.getMessage());
                ArrayList<POIDetail> tmparray = new ArrayList<>();
                tmparray.add(t);
                return tmparray;
                //return e.getMessage();
            }
            catch (IOException e)
            {
                e.printStackTrace();
                POIDetail t = new POIDetail("error");
                t.Setvariable(6,e.getMessage());
                ArrayList<POIDetail> tmparray = new ArrayList<>();
                tmparray.add(t);
                return tmparray;
                //return e.getMessage();
            }

        }



}
