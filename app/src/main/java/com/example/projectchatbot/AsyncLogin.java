package com.example.projectchatbot;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * Created by nickbanana on 2016/12/16.
 */

class AsyncLogin extends AsyncTask<String, Integer, Login_info> {


    @Override
    protected Login_info doInBackground(String... params) {
        Login_info info = new Login_info();
        Log.v("enter login","");
        OkHttpClient okHttpClient = new OkHttpClient();
        RequestBody formBody = RequestBody.create(MediaType.parse("application/x-www-form-urlencoded; charset=UTF-8"),"account=appuser&password=app123");

        Request request = new Request.Builder()
                .url("http://140.116.245.146:8080/chatbot_login/")
                .post(formBody)
                .build();

        Request request_on_csrf = new Request.Builder()
                .url("http://140.116.245.146:8080/question_data/")
                .build();

        try
        {
            okhttp3.Response response = okHttpClient.newCall(request).execute();
            if(response.code()==200)
            {
                Log.v("success?","");
                String test = response.headers().get("Set-Cookie");
                Log.v("cookie",test);
                String[] session_value = test.split(";",2);
                Log.v("session",session_value[0]);
                info.setSession_Id(session_value[0].replace("sessionid=",""));
                info.setResponse(response.body().string());
            }
            else
            {
                Log.v("error", "error in login");

            }

        }
        catch (IOException e)
        {
            e.printStackTrace();
           // return e.getMessage();
        }

        try
        {
            okhttp3.Response response = okHttpClient.newCall(request_on_csrf).execute();
            if (response.code()==200) {
                info.setCSRFtoken(response.headers().get("Set-Cookie").split(";",2)[0].replace("csrftoken=",""));
            }
            else
            {
                Log.v("error","error in getting csrf token");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        return info;
    }
}
