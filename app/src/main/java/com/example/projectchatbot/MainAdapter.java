package com.example.projectchatbot;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by nickbanana on 2016/10/17.
 */

public class MainAdapter extends BaseAdapter {
    private LayoutInflater layoutInflater;
    private List<POIDetail> poiDetailList;

    public MainAdapter(Context context, List<POIDetail> details)
    {
        layoutInflater = LayoutInflater.from(context);
        this.poiDetailList = details;
    }

    @Override
    public int getCount()
    {
        return poiDetailList.size();
    }

    @Override
    public Object getItem(int arg0)
    {
        return poiDetailList.get(arg0);
    }

    @Override
    public long getItemId(int position)
    {
        return poiDetailList.indexOf(getItem(position));
    }

    private class ViewHolder
    {
        TextView place_name;
        TextView place_address;
        public ViewHolder(TextView place_name, TextView place_address)
        {
            this.place_address = place_address;
            this.place_name = place_name;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder holder = null;
        if(convertView==null)
        {
            convertView = layoutInflater.inflate(R.layout.listitem_poi, null);
            holder = new ViewHolder((TextView) convertView.findViewById(R.id.place_name),(TextView)convertView.findViewById(R.id.place_address));
            convertView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }

        POIDetail poiDetail = (POIDetail)getItem(position);
        holder.place_name.setText(poiDetail.Getvariable(2));
        holder.place_address.setText(poiDetail.Getvariable(5));


        return convertView;
    }
}
