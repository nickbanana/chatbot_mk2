package com.example.projectchatbot;

import android.os.AsyncTask;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;

/**
 * Created by nickbanana on 2016/10/23.
 */

class AsyncRevGeocoding  extends AsyncTask<String, Void, String> {

    @Override
    protected String doInBackground(String... url)
    {
        OkHttpClient okHttpClient = new OkHttpClient();
        JsonObject object;
        Request request = new Request.Builder()
                .url(url[0])
                .build();
        String region_name="";
        try
        {
            okhttp3.Response response = okHttpClient.newCall(request).execute();
            if(response.code()==200)
            {
                object = new JsonParser().parse(response.body().string()).getAsJsonObject();
                JsonArray result_array = object.getAsJsonArray("results");
                region_name = result_array.get(0).getAsJsonObject().getAsJsonArray("address_components").get(3).getAsJsonObject().get("long_name").getAsString();

            }
            else
            {
                region_name = "error_finding_name";
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return region_name;
    }
}
