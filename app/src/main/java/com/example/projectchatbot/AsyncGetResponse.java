package com.example.projectchatbot;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;



class AsyncGetResponse extends AsyncTask<String, Integer, String> {
    @Override
    protected String doInBackground (String... params)
    {
        final String Session = "sessionid="+params[1];
        final String csrf_token = "csrftoken="+ params[2];
        final String cookie = Session + " ;" + csrf_token;
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        final Request original = chain.request();
                        final Request Authed = original.newBuilder()
                                .addHeader("Cookie",cookie)
                                .build();
                        return chain.proceed(Authed);
                    }
                })
                .build();

        HttpUrl httpUrl = new HttpUrl.Builder()
                .scheme("http")
                .host("140.116.245.146")
                .port(8080)
                .addPathSegment("question_data")
                .addPathSegment("")
                .build();


        RequestBody formBody;
        if(params.length==3)
        {
            Log.v("first execution","enter loop");
            formBody = new FormBody.Builder()
                    .add("content",params[0])
                    .add("frontId","")
                    .build();
        }
        else
        {
            Log.v("second execution","enter loop");
            formBody = new FormBody.Builder()
                    .add("content",params[0])
                    .add("frontId",params[3])
                    .build();
        }

        Request request = new Request.Builder()
                .url(httpUrl)
                .addHeader("X-CSRFToken",params[2])
                .post(formBody)
                .build();

        try
        {
            Response response = okHttpClient.newCall(request).execute();

            if(response.code()==200)
            {
                return response.body().string();
            }
            else
            {

                if (response.code()==500) {
                    response.body().close();
                    return "{\"reply\": \"Error in Server\"}";
                }
                response.body().close();
                return "{\"reply\": \"Other Error in Server communication\"}";
            }


        }catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
            return e.getMessage();
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return e.getMessage();
        }
    }
}
