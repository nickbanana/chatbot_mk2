package com.example.projectchatbot;

import android.util.Log;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import co.devcenter.android.models.SuggestionItem;

//TODO add event that will send while a task is end(to reset the ID)

class Response {

    private final String Input;
    private String identify_code;
    private AsyncGetResponse asyncGetResponse = new AsyncGetResponse();
    private Login_info login_info;
    private String ServerResponse;
    final ArrayList<SuggestionItem> Lists = new ArrayList<>();
    private ArrayList<POIDetail> POIList = new ArrayList<>();
    private AsyncTextSearch asyncTextSearch = new AsyncTextSearch();


    Response(String question, Login_info info)
    {
        this.Input = question;
        this.login_info = info;
        this.identify_code = "";
    }

    Response(String question, Login_info info, String IdCode)
    {
        this.Input = question;
        this.login_info = info;
        this.identify_code = IdCode;
    }

    String Answer()
    {

        Lists.clear();// clear previous list
        //receive answer and suggestion list from backend
        if(identify_code.equals(""))
        {
            asyncGetResponse.execute(Input,login_info.Session_Id,login_info.CSRFtoken);
        }
        else
        {
            asyncGetResponse.execute(Input,login_info.Session_Id,login_info.CSRFtoken,identify_code);
        }

        try
        {
            ServerResponse = asyncGetResponse.get();

        }
        catch (InterruptedException | ExecutionException e)
        {
            e.getStackTrace();
        }

        String ans;
        if(ServerResponse.equals("Error"))
        {
            ans = "Error on Server Response";
        }
        else
        {
            Log.v("serverresponse",ServerResponse);
            JsonObject extraction = new JsonParser().parse(ServerResponse).getAsJsonObject();
            ans = extraction.get("reply").getAsString();
            if(extraction.has("ID"))
            {
                identify_code= extraction.get("ID").getAsString();
                Log.v("response id",identify_code);
            }
            if(extraction.has("location"))
            {
                String raw_string = extraction.get("location").getAsString();
                for (String s : raw_string.split("#")) {
                    String url = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=" + s + "&key=AIzaSyBtaX--WkbbL_Aajd4_W-8knV3VwAoxZ-I";
                    asyncTextSearch.execute(url);
                    try {
                        POIDetail tmp = asyncTextSearch.get();
                        POIList.add(tmp);
                    } catch (InterruptedException | ExecutionException e) {
                        e.printStackTrace();
                    }
                }

                PlaceResponse placeResponse = new PlaceResponse(POIList);
            }
            if(extraction.has("candidant"))
            {
                String raw_string = extraction.get("candidant").getAsString();
                for (String s : raw_string.split("#")) {
                    Lists.add(new SuggestionItem(s));
                }
            }



        }

        return ans;
    }

    String getInput() {
        return Input;
    }
    String getIdentify_code() {
        return identify_code;
    }
}
