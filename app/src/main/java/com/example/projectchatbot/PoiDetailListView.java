package com.example.projectchatbot;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class PoiDetailListView extends AppCompatActivity {

    private ListView listView;
    List<POIDetail> poi_list = new ArrayList<>();
    private MainAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poi_detail_list_view);
        listView = (ListView)findViewById(R.id.mainlist);
        Intent i = getIntent();
        DataWrapper d =(DataWrapper)i.getSerializableExtra("List");
        poi_list = d.getPoiDetailsList();
        adapter = new MainAdapter(PoiDetailListView.this,poi_list);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                POIDetail choice = (POIDetail) parent.getItemAtPosition(position);
                Intent intent = new Intent(PoiDetailListView.this, POIINFO.class);
                intent.putExtra("chosen", choice);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        this.finish();
    }
}
