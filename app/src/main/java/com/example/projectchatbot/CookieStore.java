package com.example.projectchatbot;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;

/**
 * Created by nickbanana on 2016/10/14.
 */

class CookieStore implements CookieJar {
    private final Set<Cookie> cookieStore = new HashSet<>();

    @Override
    public void saveFromResponse(HttpUrl url, List<Cookie> cookies)
    {
        cookieStore.addAll(cookies);
    }

    @Override
    public List<Cookie> loadForRequest(HttpUrl url)
    {
        List<Cookie> validcookies = new ArrayList<>();
        for (Cookie cookie : cookieStore)
        {
            LogCookie(cookie);
            if(cookie.expiresAt()>System.currentTimeMillis())
            {
                validcookies.add(cookie);
            }
        }
        return validcookies;
    }

    private void LogCookie(Cookie cookie)
    {
        Log.v("Name",cookie.name());
        Log.v("value",cookie.value());

    }

}
