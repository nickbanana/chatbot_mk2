package com.example.projectchatbot;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import okhttp3.*;

/**
 * Created by chococsie on 2016/9/13.
 */
class AsyncQuery extends AsyncTask<String, Integer, String > {


    @Override
    protected String doInBackground(String... url)
    {
        OkHttpClient okHttpClient = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url[0])
                .build();

        Log.v("url",url[0]);
        try
        {
            okhttp3.Response response = okHttpClient.newCall(request).execute();

            if(response.code()==200)
            {
                return response.body().string();
            }
            else
                return "Error";
        }catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
            return e.getMessage();
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return e.getMessage();
        }
    }



}