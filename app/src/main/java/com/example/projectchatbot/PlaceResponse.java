package com.example.projectchatbot;

import java.util.ArrayList;

/**
 * Created by nickbanana on 2016/10/8.
 */

class PlaceResponse {
    final ArrayList<POIDetail> ListOfPOIDetail;
    final String SinglePlace;

    PlaceResponse(ArrayList<POIDetail> places)
    {
        this.ListOfPOIDetail = places;
        this.SinglePlace = "";
    }

    public PlaceResponse(String place)
    {
        this.SinglePlace = place;
        this.ListOfPOIDetail = new ArrayList<>();
    }


}
