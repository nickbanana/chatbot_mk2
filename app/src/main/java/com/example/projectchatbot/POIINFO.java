package com.example.projectchatbot;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class POIINFO extends FragmentActivity implements OnMapReadyCallback {

    private POIDetail Data;
    private TextView name;
    private TextView address;
    private TextView tele;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poiinfo);
        Intent i = getIntent();
        Data = (POIDetail) i.getSerializableExtra("chosen");
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map_fragment);
        mapFragment.getMapAsync(this);
        name = (TextView) findViewById(R.id.POI_name);
        address = (TextView) findViewById(R.id.POI_address);
        tele = (TextView) findViewById(R.id.POI_telephone);

        name.setText(Data.Getvariable(2));
        address.setText(Data.Getvariable(5));
        tele.setText(Data.Getvariable(7));
    }

    @Override
    public void onMapReady(GoogleMap map)
    {
        LatLng position = new LatLng(Double.valueOf(Data.Getvariable(3)),Double.valueOf(Data.Getvariable(4)));
        map.addMarker(new MarkerOptions().title(Data.Getvariable(2)).position(position));
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map.getUiSettings().setZoomGesturesEnabled(true);
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(position,16));
        map.getUiSettings().setMyLocationButtonEnabled(true);
        map.getUiSettings().setZoomControlsEnabled(true);
    }

    @Override
    public void onBackPressed()
    {
        this.finish();
    }









}
