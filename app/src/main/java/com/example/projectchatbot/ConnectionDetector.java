package com.example.projectchatbot;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;

/**
 * Created by chococsie on 2016/9/15.
 */
class ConnectionDetector {
    private final Context context;
    private final ConnectivityManager connectivityManager;
    private final NetworkInfo networkInfo;
    private boolean isConnected;




    public ConnectionDetector(Context context)
    {
        this.context = context;
        connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        networkInfo = connectivityManager.getActiveNetworkInfo();
    }

    public void NetCheck()
    {
        isConnected = networkInfo!=null && networkInfo.isConnectedOrConnecting();
        if(!isConnected)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(R.string.no_network_title);
            builder.setMessage(R.string.no_network_message);
            builder.setPositiveButton(R.string.connection_setting, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent intent;
                    intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                    context.startActivity(intent);
                }
            });
            builder.setNegativeButton(R.string.wifi_setting, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent intent;
                    intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                    context.startActivity(intent);
                }
            });
            AlertDialog alertDialog = builder.create();
            alertDialog.show();

        }
    }









}
