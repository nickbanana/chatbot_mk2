package com.example.projectchatbot;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import co.devcenter.android.ChatView;
import co.devcenter.android.ClickOnSuggestion;
import co.devcenter.android.models.UpdateResult;

//TODO Add a event listener that will reset the Identify_code when event triggered

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener{

    private ChatView chatView;
    private static final int RESULT_SPEECH = 1;
    private GoogleApiClient googleApiClient;
    private APIQueryGenerator revGeocodingGen;
    private AsyncRevGeocoding asyncRevGeocoding;
    private int REQUEST_LOCATION;
    private String Lat;
    private String Lng;
    private ArrayList<POIDetail> POIDetailList;
    private String Identify_code = "";
    private Login_info login_info = new Login_info();
    private AsyncLogin asyncLogin = new AsyncLogin();
    private Boolean mRequestingLocationUpdates;
    private LocationRequest mLocationRequest;
    private static int dialog_id= 0 ;
    String Cookie_get_status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        // set view
        setContentView(R.layout.activity_main);

        chatView = (ChatView) findViewById(R.id.view);

        FloatingActionButton floatingActionButton = chatView.getSendButton();

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                start_speak();
            }
        };

        floatingActionButton.setOnClickListener(onClickListener);

        //chatView.setChatListener(new ChatView.SimpleChatListener());
        // setting the connection
        new ConnectionDetector(this).NetCheck();


        mRequestingLocationUpdates = false;

        if(googleApiClient ==null)
        {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        createLocationRequest();

        // fetching the session cookie and
        Log.v("ready","asynclogin");
        asyncLogin.execute("test");
        try
        {
            login_info = asyncLogin.get();

        }
        catch (InterruptedException | ExecutionException e)
        {
            e.getStackTrace();

        }
        //setting up the reverse geocoding
        Log.v("info",login_info.Session_Id + " " + login_info.Response + " " + login_info.CSRFtoken);



    }




    private void start_speak()
    {
        Intent intent_speak = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent_speak.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,"en-US");

        try
        {
            startActivityForResult(intent_speak,RESULT_SPEECH);
        }
        catch (ActivityNotFoundException e)
        {
            Toast.makeText(getApplicationContext(),R.string.speech_recognition_not_support,Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode,data);

        switch (requestCode)
        {
            case RESULT_SPEECH:
            {
                if(resultCode==RESULT_OK && null != data)
                {
                    ArrayList<String> text = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    String send_txt = text.get(0);

                    if(Identify_code.length()==0)
                    {
                        Response res = new Response(send_txt,login_info);
                        SendAnswer(res);
                        Identify_code = res.getIdentify_code();
                    }
                    else
                    {
                        SendAnswer(new Response(send_txt,login_info,Identify_code));
                    }
                }
            }
        }
    }

    @Override
    protected void onStart()
    {
        googleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        stopLocationUpdates();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        if (googleApiClient.isConnected() && !mRequestingLocationUpdates) {
            startLocationUpdate();
        }
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        EventBus.getDefault().unregister(this);

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void receiveEvent(ClickOnSuggestion clickOnSuggestion)
    {
        if(Identify_code.equals(""))
        {
            Response res = new Response(clickOnSuggestion.Suggestion,login_info);
            SendAnswer(res);
            this.Identify_code = res.getIdentify_code();
        }
        else
        {
            SendAnswer(new Response(clickOnSuggestion.Suggestion,login_info,Identify_code));
        }
    }

    private void SendAnswer(Response response)
    {
        mRequestingLocationUpdates = false;
        chatView.sendMessage(response.getInput(),System.currentTimeMillis());
        chatView.newMessage(response.Answer());
        Log.v("SendAnsid",Integer.toString(dialog_id));
        chatView.updatelist(response.Lists);
        dialog_id++;
    }

   @Subscribe(threadMode = ThreadMode.MAIN)
    public void receivePlace(PlaceResponse placeResponse)
    {

        APIQueryGenerator apiQueryGenerator = new APIQueryGenerator(Lat,Lng);
        AsyncFetchQuery asyncfetchQuery = new AsyncFetchQuery();
        if (placeResponse.SinglePlace.length() > 0) {
            asyncfetchQuery.execute(apiQueryGenerator.generator(placeResponse.SinglePlace,1).toString());
            try
            {
                POIDetailList = asyncfetchQuery.get();
            }catch (InterruptedException | ExecutionException e)
            {
                e.getStackTrace();
            }
            for(int i = 0; i< POIDetailList.size() ; i++)
            {
                POIDetailList.get(i).FillParam();
            }
        }
        else
        {
            POIDetailList = placeResponse.ListOfPOIDetail;
        }

        if(POIDetailList.size()==1)
        {
            POIDetail detail = POIDetailList.get(0);
            Intent intent = new Intent (MainActivity.this,POIINFO.class);
            intent.putExtra("chosen",detail);
            startActivity(intent);
        }
        else
        {
            DataWrapper dataWrapper = new DataWrapper(POIDetailList);
            Intent intent = new Intent(MainActivity.this,PoiDetailListView.class);
            intent.putExtra("List",dataWrapper);
            startActivity(intent);
        }

    }

    @Override
    public void onConnected(Bundle connectionHint)
    {
        Location last_location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

        if(last_location !=null)
        {
            Lat = String.valueOf(last_location.getLatitude());
            Lng = String.valueOf(last_location.getLongitude());
            revGeocodingGen = new APIQueryGenerator(Lat,Lng);
        }

        if(mRequestingLocationUpdates)
        {
            startLocationUpdate();
        }
    }

    @Override
    public void onConnectionSuspended(int cause)
    {

    }

    @Override
    public void onConnectionFailed(ConnectionResult result)
    {

    }


    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }


    protected void startLocationUpdate()
    {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Check Permissions Now
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION);
        } else {
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient,mLocationRequest,this);
        }


    }

    @Override
    public void onLocationChanged(Location location) {
        Lat = String.valueOf(location.getLatitude());
        Lng = String.valueOf(location.getLongitude());
        revGeocodingGen = new APIQueryGenerator(Lat,Lng);
    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                googleApiClient, this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void GetModifyResult(UpdateResult updateResult)
    {
        int id = updateResult.getIndex()+1;
        String Input = updateResult.getContents();
        AsyncGetResponse asyncGetResponse = new AsyncGetResponse();
        asyncGetResponse.execute(Input,login_info.Session_Id,login_info.CSRFtoken);
        String respond="";
        String value="";
        JsonObject Extraction;
        try
        {
            respond = asyncGetResponse.get();

        }
        catch (InterruptedException | ExecutionException e)
        {
            e.getStackTrace();
        }


            //TODO EXTRACT the JSON
            Extraction = new JsonParser().parse(respond).getAsJsonObject();
            value = Extraction.get("reply").toString().replaceAll("\\n","\n").replaceAll("\"","");
            if(Extraction.has("id"))
            {
                Identify_code=Extraction.get("id").getAsString();
            }


        chatView.modifyMessage(value,id);
    }

    @Override
    public void onBackPressed()
    {
        this.finish();
    }
}
