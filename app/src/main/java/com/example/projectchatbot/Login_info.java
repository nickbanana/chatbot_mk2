package com.example.projectchatbot;

/**
 * Created by nickbanana on 2016/12/17.
 */

class Login_info {

    String Session_Id;
    String Response;
    String CSRFtoken;


    Login_info()
    {
        this.Session_Id = "";
        this.Response = "";
        this.CSRFtoken = "";
    }

    void setResponse(String response) {
        Response = response;
    }

    void setSession_Id(String session_Id) {
        Session_Id = session_Id;
    }

    void setCSRFtoken(String CSRFtoken) {
        this.CSRFtoken = CSRFtoken;
    }
}
