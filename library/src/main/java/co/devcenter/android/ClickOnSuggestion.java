package co.devcenter.android;

import android.support.annotation.Keep;

/**
 * Created by chococsie on 2016/9/15.
 */
@Keep
public class ClickOnSuggestion {
    public final String Suggestion;

    public ClickOnSuggestion(String Suggestion)
    {
        this.Suggestion = Suggestion;
    }
}
