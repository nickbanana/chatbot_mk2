package co.devcenter.android;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.Keep;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import co.devcenter.android.models.ChangeNotify;
import co.devcenter.android.models.ChatMessage;
import co.devcenter.android.models.ChatMessage.Type;
import co.devcenter.android.models.SuggestionItem;
import co.devcenter.android.models.UpdateResult;

@Keep
public class ChatView extends LinearLayout {

    private CardView inputBar, inputFrame;
    private ListView chatListView;
    private RecyclerView suggestion_list;
    private SuggestionListAdapter suggestionListAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private FloatingActionButton sendButton;
    private boolean sendButtonVisible;
    private ChatListener chatListener;
    private ChatViewListAdapter chatViewListAdapter;
    private int inputBarBackgroundColor, inputBarInsetLeft, inputBarInsetTop, inputBarInsetRight, inputBarInsetBottom;
    private int inputTextSize, inputTextColor, inputHintColor;
    private int sendButtonBackgroundTint, sendButtonIconTint, sendButtonElevation;
    private Drawable sendButtonIcon, buttonDrawable;
    private TypedArray attributes, textAppearanceAttributes;
    private Context context;


    public ChatView(Context context) {
        this(context, null);
    }

    public ChatView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ChatView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }


    private void init(Context context, AttributeSet attrs, int defStyleAttr) {

        setOrientation(VERTICAL);
        LayoutInflater.from(getContext()).inflate(R.layout.chat_view, this, true);
        this.context = context;
        initializeViews();
        getXMLAttributes(attrs, defStyleAttr);
        setViewAttributes();
        chatViewListAdapter = new ChatViewListAdapter(context);
        chatListView.setAdapter(chatViewListAdapter);
        suggestionListAdapter = new SuggestionListAdapter(new ArrayList<SuggestionItem>());
        layoutManager = new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false);
        suggestion_list.setAdapter(suggestionListAdapter);
        suggestion_list.setLayoutManager(layoutManager);
        suggestion_list.setVisibility(GONE); // won't appear until need to use
        chatListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ChatMessage choice = (ChatMessage) parent.getItemAtPosition(position);
                if(position%2==0)
                {
                    Intent intent = new Intent(getContext(),EditMessage.class);
                    intent.putExtra("choice",choice);
                    intent.putExtra("choiceId",position);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getContext().startActivity(intent);
                }
                else
                {
                    if (choice.getLink().length()!=0) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(choice.getLink()));
                        getContext().startActivity(intent);
                    }
                }
                Log.v("position",Integer.toString(position));


            }
        });
        EventBus.getDefault().register(this);


        //setButtonOnClickListener();
        //setUserTypingListener();
        //setUserStoppedTypingListener();
    }

    private void initializeViews() {
        chatListView = (ListView) findViewById(R.id.chat_list);
        inputBar = (CardView) findViewById(R.id.input_bar);
        //inputFrame = (CardView) findViewById(R.id.input_frame);
        //inputEditText = (EditText) findViewById(R.id.input_edit_text);
        sendButton = (FloatingActionButton) findViewById(R.id.sendButton);
        suggestion_list = (RecyclerView) findViewById(R.id.suggestion_list);
    }

    private void getXMLAttributes(AttributeSet attrs, int defStyleAttr) {
        attributes = context.obtainStyledAttributes(attrs, R.styleable.ChatView, defStyleAttr, R.style.ChatViewDefault);
        getAttributesForInputBar();
        getAttributesForInputText();
        getAttributesForSendButton();
        attributes.recycle();
    }

    private void setViewAttributes() {
        setInputBarAttributes();
        setSendButtonAttributes();
    }

    private void getAttributesForInputBar() {
        inputBarBackgroundColor = attributes.getColor(R.styleable.ChatView_inputBarBackgroundColor, -1);
        inputBarInsetLeft = attributes.getDimensionPixelSize(R.styleable.ChatView_inputBarInsetLeft, 0);
        inputBarInsetTop = attributes.getDimensionPixelSize(R.styleable.ChatView_inputBarInsetTop, 0);
        inputBarInsetRight = attributes.getDimensionPixelSize(R.styleable.ChatView_inputBarInsetRight, 0);
        inputBarInsetBottom = attributes.getDimensionPixelSize(R.styleable.ChatView_inputBarInsetBottom, 0);
    }

    private void setInputBarAttributes() {
        inputBar.setCardBackgroundColor(inputBarBackgroundColor);
        inputBar.setContentPadding(inputBarInsetLeft, inputBarInsetTop, inputBarInsetRight, inputBarInsetBottom);
    }



    private void getAttributesForInputText() {
        setInputTextDefaults();
        if (hasStyleResourceSet()) {
            final int textAppearanceId = attributes.getResourceId(R.styleable.ChatView_inputTextAppearance, 0);
            textAppearanceAttributes = getContext().obtainStyledAttributes(textAppearanceId, R.styleable.ChatViewInputTextAppearance);
            setInputTextSize();
            setInputTextColor();
            setInputHintColor();
            textAppearanceAttributes.recycle();
        }
        overrideTextStylesIfSetIndividually();
    }

    private void getAttributesForSendButton() {
        sendButtonVisible = attributes.getBoolean(R.styleable.ChatView_sendBtnVisible, true);
        sendButtonBackgroundTint = attributes.getColor(R.styleable.ChatView_sendBtnBackgroundTint, -1);
        sendButtonIconTint = attributes.getColor(R.styleable.ChatView_sendBtnIconTint, Color.WHITE);
        sendButtonElevation = attributes.getDimensionPixelSize(R.styleable.ChatView_sendBtnElevation, 0);
        sendButtonIcon = attributes.getDrawable(R.styleable.ChatView_sendBtnIcon);
    }

    private void setSendButtonAttributes() {
        if (!sendButtonVisible) sendButton.setVisibility(GONE);
        sendButton.setBackgroundTintList(ColorStateList.valueOf(sendButtonBackgroundTint));
        sendButton.setImageDrawable(sendButtonIcon);
        buttonDrawable = DrawableCompat.wrap(sendButton.getDrawable());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            sendButton.setElevation(sendButtonElevation);
        }
        buttonDrawable.setColorFilter(sendButtonIconTint, PorterDuff.Mode.SRC_IN);
    }





    private boolean hasStyleResourceSet() {
        return attributes.hasValue(R.styleable.ChatView_inputTextAppearance);
    }

    private void setInputTextDefaults() {
        inputTextSize = context.getResources().getDimensionPixelSize(R.dimen.default_input_text_size);
        inputTextColor = ContextCompat.getColor(context, R.color.black);
        inputHintColor = ContextCompat.getColor(context, R.color.main_color_gray);
    }

    private void setInputTextSize() {
        if (textAppearanceAttributes.hasValue(R.styleable.ChatView_inputTextSize)) {
            inputTextSize = attributes.getDimensionPixelSize(R.styleable.ChatView_inputTextSize, inputTextSize);
        }
    }

    private void setInputTextColor() {
        if (textAppearanceAttributes.hasValue(R.styleable.ChatView_inputTextColor)) {
            inputTextColor = attributes.getColor(R.styleable.ChatView_inputTextColor, inputTextColor);
        }
    }

    private void setInputHintColor() {
        if (textAppearanceAttributes.hasValue(R.styleable.ChatView_inputHintColor)) {
            inputHintColor = attributes.getColor(R.styleable.ChatView_inputHintColor, inputHintColor);
        }
    }

    private void overrideTextStylesIfSetIndividually() {
        inputTextSize = (int) attributes.getDimension(R.styleable.ChatView_inputTextSize, inputTextSize);
        inputTextColor = attributes.getColor(R.styleable.ChatView_inputTextColor, inputTextColor);
        inputHintColor = attributes.getColor(R.styleable.ChatView_inputHintColor, inputHintColor);
    }





    @Override
    protected boolean addViewInLayout(View child, int index, ViewGroup.LayoutParams params) {
        return super.addViewInLayout(child, index, params);
    }

    public void setChatListener(ChatListener chatListener) {
        this.chatListener = chatListener;
    }

    public void sendMessage(String message, long stamp) {
        ChatMessage chatMessage = new ChatMessage(message, stamp, Type.SENT);
        //if (chatListener != null && chatListener.sendMessage(message, stamp)) {
        chatViewListAdapter.addMessage(chatMessage);
        //}
        suggestion_list.setVisibility(GONE);
    }

    public void newMessage(String message) {
        ChatMessage chatMessage = new ChatMessage(message, System.currentTimeMillis(), Type.RECEIVED);
        Pattern pattern = Pattern.compile("<a href=\"([^\"]*)\">([^<]*)");
        Matcher matcher = pattern.matcher(message);

        if (matcher.find()) {
            Log.v("full",message);
            chatMessage.setLink(matcher.group(1));
            chatMessage.setMessage("點選連結:"+matcher.group(2));
        }
        chatViewListAdapter.addMessage(chatMessage);
        suggestion_list.setVisibility(VISIBLE);
        notifyMessageReceivedListener(chatMessage);
    }

    public void modifyMessage(String new_message, int index)
    {
        chatViewListAdapter.changeMessage(new_message, index);
    }

    public void newMessage(ChatMessage chatMessage) {
        chatViewListAdapter.addMessage(chatMessage);
        notifyMessageReceivedListener(chatMessage);
    }

    private void notifyMessageReceivedListener(ChatMessage chatMessage) {
        if (chatListener != null)
            chatListener.onMessageReceived(chatMessage.getMessage(), chatMessage.getTimestamp());
    }

    //public EditText getInputEditText() {
    //    return inputEditText;
    //}

    public FloatingActionButton getSendButton() {
        return sendButton;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ChangeNotify changeNotify)
    {
        chatViewListAdapter.changeMessage(changeNotify.getMod(),changeNotify.getId());
        EventBus.getDefault().post(new UpdateResult(changeNotify.getId(),changeNotify.getMod()));
    }



    public class ChatViewListAdapter extends BaseAdapter {
        public final int STATUS_SENT = 0;
        public final int STATUS_RECEIVED = 1;
        final ArrayList<ChatMessage> chatMessages;
        final Context context;
        final LayoutInflater inflater;

        public ChatViewListAdapter(Context context) {
            this.chatMessages = new ArrayList<>();
            this.context = context;
            this.inflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return chatMessages.size();
        }

        @Override
        public Object getItem(int position) {
            return chatMessages.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemViewType(int position) {
            return chatMessages.get(position).getType().ordinal();
        }

        @Override
        public int getViewTypeCount() {
            return 2;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            int type = getItemViewType(position);
            if (convertView == null) {
                switch (type) {
                    case STATUS_SENT:
                        convertView = inflater.inflate(R.layout.chat_item_sent, parent, false);
                        break;
                    case STATUS_RECEIVED:
                        convertView = inflater.inflate(R.layout.chat_item_rcv, parent, false);
                        break;
                }

                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.getMessageTextView().setText(chatMessages.get(position).getMessage());
            holder.getTimestampTextView().setText(chatMessages.get(position).getFormattedTime());

            return convertView;
        }

        public void addMessage(ChatMessage message) {
            chatMessages.add(message);
            notifyDataSetChanged();
        }

        void changeMessage(String to_change, int index)
        {
            chatMessages.get(index).setMessage(to_change);
            notifyDataSetChanged();
        }

        public ArrayList<ChatMessage> pullList()
        {
            return chatMessages;
        }



        class ViewHolder {
            final View row;
            TextView messageTextView;
            TextView timestampTextView;

            public ViewHolder(View convertView) {
                row = convertView;
            }

            public TextView getMessageTextView() {
                if (messageTextView == null) {
                    messageTextView = (TextView) row.findViewById(R.id.message_text_view);
                }
                return messageTextView;
            }

            public TextView getTimestampTextView() {
                if (timestampTextView == null) {
                    timestampTextView = (TextView) row.findViewById(R.id.timestamp_text_view);
                }

                return timestampTextView;
            }
        }
    }

    public void RemoveSubscriber()
    {
        EventBus.getDefault().unregister(this);
    }


    public interface ChatListener {

        void userIsTyping();

        void userHasStoppedTyping();

        void onMessageReceived(String message, long timestamp);

        boolean sendMessage(String message, long timestamp);
    }



    public static class SuggestionListAdapter extends RecyclerView.Adapter<SuggestionListAdapter.ViewHolder>
    {
        ArrayList<SuggestionItem> suggestionItemList;
        Context context;
        LayoutInflater inflater;


        public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
        {
            final CardView CV;
            final TextView content;
            public final MyOnClickListener myOnClickListener;
            public ViewHolder(View v,MyOnClickListener listener)
            {
                super(v);
                myOnClickListener = listener;

                CV = (CardView)v.findViewById(R.id.suggestion_block);
                content = (TextView)v.findViewById(R.id.suggestion_block_text);
                v.setOnClickListener(this);
            }

            @Override
            public void onClick(View v)
            {
                myOnClickListener.clickOnView(v,getLayoutPosition());
            }

            public interface MyOnClickListener
            {
                void clickOnView(View v, int position);
            }


        }

        public SuggestionListAdapter(ArrayList<SuggestionItem> list)
        {
            this.suggestionItemList = list;
        }

        @Override
        public SuggestionListAdapter.ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType)
        {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.popup_item,parent,false);
            return new ViewHolder(v, new ViewHolder.MyOnClickListener() {
                @Override
                public void clickOnView(View v1, int position) {
                    String response = suggestionItemList.get(position).getItemName();
                    //Toast.makeText(parent.getContext(),response, Toast.LENGTH_LONG).show();
                    EventBus.getDefault().post(new ClickOnSuggestion(response));




                }
            });
        }

        @Override
        public void onBindViewHolder(ViewHolder vh, int position)
        {
            vh.content.setText(suggestionItemList.get(position).getItemName());
        }

        @Override
        public int getItemCount()
        {
            return suggestionItemList.size();
        }

        @Override
        public void onAttachedToRecyclerView(RecyclerView recyclerView)
        {
            super.onAttachedToRecyclerView(recyclerView);
        }

        public void addsuggestion(String suggestion)
        {
            suggestionItemList.add(new SuggestionItem(suggestion));
            notifyDataSetChanged();
        }

        public void UpdateList(ArrayList<SuggestionItem> list)
        {
            suggestionItemList = list;
            notifyDataSetChanged();
        }

    }

    public void updatelist(ArrayList<SuggestionItem> list)
    {
        suggestionListAdapter.UpdateList(list);
    }





}
