package co.devcenter.android.models;

import android.support.annotation.Keep;

/**
 * Created by chococsie on 2016/9/11.
 */
@Keep
public class SuggestionItem {
    private String ItemName;

    public SuggestionItem(String itemName)
    {
        this.ItemName = itemName;
    }

    public String getItemName() {
        return ItemName;
    }

    public void setItemName(String itemName) {
        this.ItemName = itemName;
    }
}
