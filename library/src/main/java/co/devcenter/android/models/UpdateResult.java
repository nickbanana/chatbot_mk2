package co.devcenter.android.models;

import android.support.annotation.Keep;

/**
 * Created by nickbanana on 2016/10/26.
 */
@Keep
public class UpdateResult {
    final int index;
    final String contents;

    public UpdateResult(int index, String contents)
    {
        this.contents = contents;
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public String getContents() {
        return contents;
    }
}
