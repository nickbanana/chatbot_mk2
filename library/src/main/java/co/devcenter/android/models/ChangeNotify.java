package co.devcenter.android.models;

import android.support.annotation.Keep;

/**
 * Created by nickbanana on 2016/10/25.
 */
@Keep
public class ChangeNotify {
    final int id;
    final String mod;
    public ChangeNotify(int id,String mod)
    {
        this.id=id;
        this.mod= mod;
    }

    public int getId() {
        return id;
    }

    public String getMod() {
        return mod;
    }
}
