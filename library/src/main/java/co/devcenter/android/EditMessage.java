package co.devcenter.android;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import co.devcenter.android.models.ChangeNotify;
import co.devcenter.android.models.ChatMessage;

public class EditMessage extends AppCompatActivity {
    ChatMessage chatMessage;
    Button Accept;
    Button Cancel;
    EditText contents;
    int id;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_editor);
        Intent intent = getIntent();
        chatMessage = (ChatMessage) intent.getSerializableExtra("choice");
        id = intent.getIntExtra("choiceId",0);
        contents = (EditText) findViewById(R.id.edit_text);
        contents.setHint(chatMessage.getMessage());
        Accept = (Button) findViewById(R.id.confirm);
        Cancel = (Button) findViewById(R.id.cancel);
        Accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                change_message();
            }
        });
        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                return_to_main();
            }
        });

    }

    private void change_message()
    {
        EventBus.getDefault().post(new ChangeNotify(id,contents.getText().toString()));
        finish();
    }

    private void return_to_main()
    {
        finish();
    }


}
